# Environment Variables

## Local Environment

env_file path: `/config/dev.env`
generate env file from `/config/dev.env.sample`

and If you want to use another path then you will have to change path in `package.json(line 8)` for dev Script.

## Production Environment:

In Production Environment we dont need to provide path for Environment Variables (almost) so no need to worry about that

Now, Let's check the list of variables for Production Environment...

> Variables:

```
    PORT -> Port Number on local machine (No need to specify on Production Server)
    MONGODB_PRODUCTION_URL -> URL for MongoDB Database
    SENDGRID_API_KEY -> SendGrid API key (SendGrid: It provides NodeJS API to send email from application)
    ADMIN_EMAIL -> Email_Id that you want to set as your application admin
    SENDGRID_SENDER_EMAIL -> Email_Id that you want to send email from (Email_Id must be authorized from SendGrid)
```
