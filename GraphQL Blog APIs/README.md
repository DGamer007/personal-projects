# GraphQL Blog App

A GraphQL Application which provides all the necessary APIs to get started with a basic Blog Application.

## Overview

This GraphQL Blog Application is as name suggests, a GraphQL application which provides APIs that are needed for getting started with a basic blog application. It uses **Apollo-Server-Express** as a GraphQL Server, **MongoDB** for Database, **Prisma** as ORM, **Express** as underlying web Server, **JWT** for Authentication. The Schema is well designed to handle Errors as explained in [this](https://sachee.medium.com/200-ok-error-handling-in-graphql-7ec869aec9bc) article.

This Application also supports **GraphQL Subscriptions** and uses **graphql-ws** as Websocket Implementation for Subscriptions.

You will find the Whole Schema in [here](./Schema.md)

#### Contributors:

> [Dhruv Prajapati](https://github.com/DGamer007)
