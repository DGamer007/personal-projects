# ChatApp

As name suggests, This NodeJS Application will allow users to access a Room in which users will be able to have a group chat.

## Overview

This Application uses **Socket.io** module to send and receive data between server and client.

Using this Web-Application Users will be able to have group chat with people in the same room and also Users can share their **Current Location** in Chat.

#### Contributors:

> [Dhruv Prajapati](https://github.com/DGamer007)
