# Indecision App

A Basic ReactJS Web Application, which will choose 1 option among many of them provided by user.

## Overview

This Web Application uses **React** Framework. So, basically this Project is having some React Components and also self-configured **Webpack** and **Babel** Configurations.

#### Contributors:

> [Dhruv Prajapati](https://github.com/DGamer007)
