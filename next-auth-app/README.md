# DGamer' Next-Auth App

A Basic NextJS Application which explains the functionality of **NextAuth** in a precise way.

## Overview

This is a Basic NextJS-NextAuth Application. Typically Users can authenticate themselves in this App and this application has this extra feature of changing the Password of Account. For now, This application has Custom Credentials based Authentication Unabled and is using MongoDB to store the User Data.

#### Contributors:

> [Dhruv Prajapati](https://github.com/DGamer007)
