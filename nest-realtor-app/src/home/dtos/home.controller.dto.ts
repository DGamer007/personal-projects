import { Exclude } from 'class-transformer';
import { ArrayNotEmpty, IsArray, IsEnum, IsNotEmpty, IsNumber, IsOptional, IsPositive, IsString } from 'class-validator';
import { PropertyType } from '@prisma/client';
import { UserResponseDto } from '../../user/dtos/user.controller.dto';

export class HomeResponseDto {
    id: string;
    address: string;
    numberOfBedrooms: number;
    numberOfBathrooms: number;
    city: string;
    listedDate: Date;
    price: number;
    landSize: number;
    propertyType: PropertyType;
    realtor: UserResponseDto;

    @Exclude()
    realtorId: string;
    @Exclude()
    createdAt: Date;
    @Exclude()
    updatedAt: Date;

    constructor(partial: Partial<HomeResponseDto>) {
        partial.realtor && (partial.realtor = new UserResponseDto(partial.realtor));
        Object.assign(this, partial);
    }
}

export class MessageResponseDto {
    id: string;
    message: string;
    homeId: string;
    buyer: UserResponseDto;

    @Exclude()
    buyerId: string;

    @Exclude()
    realtorId: string;

    constructor(partial: Partial<MessageResponseDto>) {
        partial.buyer = new UserResponseDto(partial.buyer);
        Object.assign(this, partial);
    }
}

export class CreateHomeDto {
    @IsString()
    @IsNotEmpty()
    address: string;

    @IsNumber()
    @IsPositive()
    numberOfBedrooms: number;

    @IsNumber()
    @IsPositive()
    numberOfBathrooms: number;

    @IsString()
    @IsNotEmpty()
    city: string;

    listedDate: Date;

    @IsNumber()
    @IsPositive()
    price: number;

    @IsNumber()
    @IsPositive()
    landSize: number;

    @IsEnum(PropertyType)
    propertyType: PropertyType;

    @IsArray()
    @ArrayNotEmpty()
    @IsString({ each: true })
    images: string[];
}

export class UpdateHomeDto {
    @IsString()
    @IsNotEmpty()
    @IsOptional()
    address: string;

    @IsNumber()
    @IsPositive()
    @IsOptional()
    numberOfBedrooms: number;

    @IsNumber()
    @IsPositive()
    @IsOptional()
    numberOfBathrooms: number;

    @IsString()
    @IsNotEmpty()
    @IsOptional()
    city: string;

    @IsNumber()
    @IsPositive()
    @IsOptional()
    price: number;

    @IsNumber()
    @IsPositive()
    @IsOptional()
    landSize: number;

    @IsEnum(PropertyType)
    @IsOptional()
    propertyType: PropertyType;

    @IsArray()
    @ArrayNotEmpty()
    @IsString({ each: true })
    @IsOptional()
    images: string[];
}

export class InquireDto {
    @IsString()
    @IsNotEmpty()
    message: string;
}

export class GetHomeQueryParams {

    @IsOptional()
    @IsString()
    @IsNotEmpty()
    city: string;

    @IsOptional()
    price: Range;

    @IsOptional()
    landSize: Range;

    @IsOptional()
    numberOfBedrooms: Range;

    @IsOptional()
    numberOfBathrooms: Range;

    @IsOptional()
    propertyType: PropertyType;

    constructor(partial: Partial<GetHomeQueryParams>) {
        console.log(partial);

        if (partial) {
            partial.propertyType && (this.propertyType = partial.propertyType);
            partial.city && (this.city = partial.city);

            keys.forEach(({ partialKey, key, foo, op }) => {
                if (partial[partialKey]) {
                    this[key] ||= {};
                    this[key][op] = eval(foo).call(null, partial[partialKey]);
                }
            });

            console.log(this);


            // if (partial.maxPrice) {
            //     this.price ||= {};
            //     this.price.lte = parseFloat(partial.maxPrice);
            // }

            // if (partial.minPrice) {
            //     this.price ||= {};
            //     this.price.gte = parseFloat(partial.minPrice);
            // }

            // if (partial.maxBedrooms) {
            //     this.numberOfBedrooms ||= {};
            //     this.numberOfBedrooms.lte = parseInt(partial.maxBedrooms);
            // }

            // if (partial.minBedrooms) {
            //     this.numberOfBedrooms ||= {};
            //     this.numberOfBedrooms.gte = parseInt(partial.minBedrooms);
            // }

            // if (partial.maxBathrooms) {
            //     this.numberOfBathrooms ||= {};
            //     this.numberOfBathrooms.lte = parseFloat(partial.maxBathrooms);
            // }

            // if (partial.minBathrooms) {
            //     this.numberOfBathrooms ||= {};
            //     this.numberOfBathrooms.gte = parseFloat(partial.minBathrooms);
            // }

            // if (partial.maxLandSize) {
            //     this.landSize ||= {};
            //     this.landSize.lte = parseFloat(partial.maxLandSize);
            // }

            // if (partial.minLandSize) {
            //     this.landSize ||= {};
            //     this.landSize.gte = parseFloat(partial.minLandSize);
            // }
        }
    }
}

export type QueryParams = {
    city: string;
    maxPrice: string;
    minPrice: string;
    maxLandSize: string;
    minLandSize: string;
    maxBedrooms: string;
    minBedrooms: string;
    maxBathrooms: string;
    minBathrooms: string;
    propertyType: PropertyType;
};

const keys = [
    { partialKey: 'maxPrice', op: 'lte', key: 'price', foo: 'parseFloat' },
    { partialKey: 'minPrice', op: 'gte', key: 'price', foo: 'parseFloat' },
    { partialKey: 'maxBedrooms', op: 'lte', key: 'numberOfBedrooms', foo: 'parseInt' },
    { partialKey: 'minBedrooms', op: 'gte', key: 'numberOfBedrooms', foo: 'parseInt' },
    { partialKey: 'maxBathrooms', op: 'lte', key: 'numberOfBathrooms', foo: 'parseFloat' },
    { partialKey: 'minBathrooms', op: 'gte', key: 'numberOfBathrooms', foo: 'parseFloat' },
    { partialKey: 'maxLandSize', op: 'lte', key: 'landSize', foo: 'parseFloat' },
    { partialKey: 'minLandSize', op: 'gte', key: 'landSize', foo: 'parseFloat' },
];

// export type QueryParams = {
//     city?: string;
//     price?: Range;
//     numberOfBedrooms?: Range;
//     numberOfBathrooms?: Range;
//     landSize?: Range;
//     propertyType: PropertyType;
// };

type Range = {
    gte?: number;
    lte?: number;
};

export type CreateHome = {
    address: string;
    numberOfBedrooms: number;
    numberOfBathrooms: number;
    city: string;
    listedDate: Date;
    price: number;
    landSize: number;
    propertyType: PropertyType;
    images: string[];
};

export type UpdateHome = {
    address?: string;
    numberOfBedrooms?: number;
    numberOfBathrooms?: number;
    city?: string;
    price?: number;
    landSize?: number;
    propertyType?: PropertyType;
};