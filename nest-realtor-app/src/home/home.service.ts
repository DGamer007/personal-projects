import { Injectable, NotFoundException } from '@nestjs/common';
import { UnauthorizedException } from '@nestjs/common/exceptions';
import { PrismaService } from '../prisma/prisma.service';
import { CreateHome, GetHomeQueryParams, HomeResponseDto, MessageResponseDto, QueryParams, UpdateHome } from './dtos/home.controller.dto';

@Injectable()
export class HomeService {

    constructor(private readonly prisma: PrismaService) { }

    async getHomes(queryParams: GetHomeQueryParams): Promise<HomeResponseDto[]> {
        const homes = await this.prisma.home.findMany({
            where: { ...queryParams }
        });
        if (!homes.length) throw new NotFoundException();

        return homes.map(home => new HomeResponseDto(home));
    }

    async getHome(id: string): Promise<HomeResponseDto> {
        const home = await this.prisma.home.findUnique({
            where: { id },
            include: { realtor: true }
        });
        if (!home) throw new NotFoundException();

        return new HomeResponseDto(home);
    }

    async createHome(body: CreateHome, realtorId: string): Promise<HomeResponseDto> {
        const home = await this.prisma.home.create({
            data: {
                ...body,
                images: {
                    createMany: { data: body.images.map(image => ({ url: image })) }
                },
                realtor: { connect: { id: realtorId } }
            }
        });

        return new HomeResponseDto(home);
    }

    async updateHome(homeId: string, data: UpdateHome, userId: string): Promise<HomeResponseDto> {
        let home = await this.prisma.home.findFirst({ where: { id: homeId } });
        if (!home) throw new NotFoundException();

        if (home.realtorId !== userId) throw new UnauthorizedException();

        home = await this.prisma.home.update({ where: { id: homeId }, data });
        return new HomeResponseDto(home);
    }

    async deleteHome(homeId: string, userId: string) {
        const home = await this.prisma.home.findFirst({ where: { id: homeId } });
        if (!home) throw new NotFoundException();

        if (home.realtorId !== userId) throw new UnauthorizedException();

        await this.prisma.home.delete({ where: { id: homeId } });
        return;
    }

    async inquire(homeId: string, message: string, userId: string) {
        const home = await this.prisma.home.findUnique({ where: { id: homeId } });
        if (!home) throw new NotFoundException();

        await this.prisma.message.create({
            data: {
                message,
                home: { connect: { id: homeId } },
                buyer: { connect: { id: userId } },
                realtor: { connect: { id: home.realtorId } }
            }
        });

        return;
    }

    async inquiries(homeId: string, realtorId: string) {
        const inquiries = await this.prisma.message.findMany({
            where: { homeId, realtorId },
            include: {
                buyer: true
            }
        });
        if (!inquiries?.length) throw new NotFoundException();

        return inquiries.map(inquiry => new MessageResponseDto(inquiry));
    }
}
