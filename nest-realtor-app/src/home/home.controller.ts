import { Controller, Get, Post, Put, Delete, Param, Query, Body, ParseIntPipe, BadRequestException, ParseUUIDPipe } from '@nestjs/common';
import { CreateHomeDto, GetHomeQueryParams, HomeResponseDto, InquireDto, UpdateHomeDto } from './dtos/home.controller.dto';
import User from '../user/decorators/user.decorator';
import { HomeService } from './home.service';
import { JWTPayload } from '../user/auth/dtos/auth.controller.dto';
import { Roles } from '../user/auth/decorators/auth.decorator';
import { UserType } from '@prisma/client';

@Controller('home')
export class HomeController {

    constructor(private readonly homeService: HomeService) { }

    @Get()
    getHomes(
        @Query() queryParams: GetHomeQueryParams
    ) {
        console.log(queryParams);

        return this.homeService.getHomes(queryParams);
    }

    @Get(':id')
    getHome(
        @Param('id', ParseUUIDPipe) id: string
    ) {
        return this.homeService.getHome(id);
    }

    @Roles(UserType.REALTOR)
    @Post()
    createHome(
        @Body() body: CreateHomeDto,
        @User() user: JWTPayload
    ) {
        if (!user) throw new BadRequestException();
        return this.homeService.createHome(body, user.id);
    }

    @Roles(UserType.REALTOR)
    @Put(':id')
    updateHome(
        @Param('id', ParseUUIDPipe) id: string,
        @Body() body: UpdateHomeDto,
        @User() user: JWTPayload
    ) {
        if (!user) throw new BadRequestException();
        return this.homeService.updateHome(id, body, user.id);
    }

    @Roles(UserType.REALTOR)
    @Delete(':id')
    deleteHome(
        @Param('id', ParseUUIDPipe) id: string,
        @User() user: JWTPayload
    ) {
        if (!user) throw new BadRequestException();
        return this.homeService.deleteHome(id, user.id);
    }

    @Roles(UserType.BUYER)
    @Post(':id/inquire')
    inquire(
        @Param('id', ParseUUIDPipe) id: string,
        @User() user: JWTPayload,
        @Body() { message }: InquireDto
    ) {
        if (!user) throw new BadRequestException();
        return this.homeService.inquire(id, message, user.id);
    }

    @Roles(UserType.REALTOR)
    @Get(':id/inquiries')
    getInquiries(
        @Param('id', ParseUUIDPipe) id: string,
        @User() user: JWTPayload,
    ) {
        if (!user) throw new BadRequestException();
        return this.homeService.inquiries(id, user.id);
    }

}
