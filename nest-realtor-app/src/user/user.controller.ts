import { Get, Controller, UnauthorizedException } from '@nestjs/common';
import { JWTPayload } from './auth/dtos/auth.controller.dto';
import User from './decorators/user.decorator';
import { UserService } from './user.service';

@Controller('user')
export class UserController {
    constructor(private readonly userService: UserService) { }

    @Get('/me')
    async me(
        @User() user: JWTPayload
    ) {
        if (!user) throw new UnauthorizedException();
        return this.userService.me(user.id);
    }
}
