import { UserType } from '@prisma/client';
import { Exclude } from 'class-transformer';

export class UserResponseDto {
    id: string;
    firstName: string;
    lastName: string;
    email: string;
    phone: string;

    @Exclude()
    userType: UserType;

    @Exclude()
    password: string;

    @Exclude()
    createdAt: Date;

    @Exclude()
    updatedAt: Date;

    constructor(user: UserResponseDto) {
        Object.assign(this, user);
    }
};