import { Injectable } from '@nestjs/common';
import { PrismaService } from '../prisma/prisma.service';

@Injectable()
export class UserService {

    constructor(private readonly prisma: PrismaService) { }

    async me(id: string) {
        const user = await this.prisma.user.findUnique({
            where: { id },
            include: {
                homes: true,
                buyerMessages: true,
                realtorMessages: true
            }
        });

        return user;
    }
}
