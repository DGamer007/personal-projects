import { ExecutionContext } from '@nestjs/common/interfaces/features/execution-context.interface';
import { CallHandler, NestInterceptor } from '@nestjs/common/interfaces/features/nest-interceptor.interface';
import * as jwt from 'jsonwebtoken';
import { JWTPayload } from '../auth/dtos/auth.controller.dto';

class UserInterceptor implements NestInterceptor {
    intercept(context: ExecutionContext, handler: CallHandler) {
        const req = context.switchToHttp().getRequest();
        const token = req?.headers?.authorization?.replace('Bearer ', '');

        if (token) {
            const decoded = this.decodeToken(token);
            req.user = { id: decoded.id };
        }

        return handler.handle();
    }

    private decodeToken(token: string): JWTPayload {
        return jwt.decode(token) as JWTPayload;
    }
}

export default UserInterceptor;