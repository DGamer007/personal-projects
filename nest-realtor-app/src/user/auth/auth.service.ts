import { HttpException, Injectable, ConflictException, NotAcceptableException } from '@nestjs/common';
import * as bcrypt from 'bcryptjs';
import * as jwt from 'jsonwebtoken';
import { UserType } from '@prisma/client';
import { PrismaService } from '../../prisma/prisma.service';
import { JWTPayload, Signin, Signup, AuthResponseDto, RegisterSuperAdmin } from './dtos/auth.controller.dto';
import { UserResponseDto } from '../dtos/user.controller.dto';

@Injectable()
export class AuthService {

    constructor(private readonly prisma: PrismaService) { }

    async registerSuperAdmin({ email, password, ...rest }: RegisterSuperAdmin) {
        let user = await this.prisma.user.findFirst({ where: { userType: UserType.SUPERADMIN } });
        if (user) throw new ConflictException();

        user = await this.prisma.user.findUnique({ where: { email } });
        if (user) throw new ConflictException();

        const hashedPassword = await bcrypt.hash(password, 10);

        user = await this.prisma.user.create({
            data: {
                ...rest,
                email,
                password: hashedPassword,
                userType: UserType.SUPERADMIN
            }
        });

        return new AuthResponseDto({
            token: this.generateAuthToken({ id: user.id }),
            user: new UserResponseDto(user)
        });

    }

    async signup(body: Signup, userType: UserType): Promise<AuthResponseDto> {
        if (body.email.match(/^(superadmin@)(.)*$/)) throw new NotAcceptableException();

        let user = await this.prisma.user.findUnique({ where: { email: body.email } });
        if (user) throw new ConflictException();

        const hashedPassword = await bcrypt.hash(body.password, 10);
        delete body.registrationKey;

        user = await this.prisma.user.create({
            data: {
                ...body,
                password: hashedPassword,
                userType
            }
        });

        return new AuthResponseDto({
            token: this.generateAuthToken({ id: user.id }),
            user: new UserResponseDto(user)
        });
    }

    async signin({ email, password }: Signin): Promise<AuthResponseDto> {
        const user = await this.prisma.user.findUnique({ where: { email } });
        if (!user) throw new HttpException("Invalid Credentials", 400);

        const isValid = await bcrypt.compare(password, user.password);
        if (!isValid) throw new HttpException("Invalid Credentials", 400);

        return new AuthResponseDto({
            token: this.generateAuthToken({ id: user.id }),
            user: new UserResponseDto(user)
        });
    }

    async generateRegistrationKey(email: string, userType: UserType) {
        return {
            registrationKey: await bcrypt.hash(`${userType}-${email}-${process.env.KEY_SECRET}`, 10)
        };
    }

    private generateAuthToken(payload: JWTPayload) {
        return jwt.sign(payload, process.env.JWT_SECRET, {
            expiresIn: 3600
        });
    }
}