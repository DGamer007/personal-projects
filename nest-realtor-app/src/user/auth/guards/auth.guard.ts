import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import * as jwt from 'jsonwebtoken';
import { PrismaService } from '../../../prisma/prisma.service';
import { JWTPayload } from '../dtos/auth.controller.dto';

@Injectable()
export class AuthGuard implements CanActivate {
    constructor(
        private readonly reflector: Reflector,
        private readonly prismaService: PrismaService
    ) { }

    async canActivate(context: ExecutionContext) {
        const roles = this.reflector.getAllAndOverride('roles', [
            context.getHandler(),
            context.getClass()
        ]);

        if (roles?.length) {
            const req = context.switchToHttp().getRequest();
            const token = req.headers?.authorization?.replace('Bearer ', '');
            if (!token) return false;

            try {
                const decoded = this.verifyToken(token);

                const user = await this.prismaService.user.findUnique({ where: { id: decoded.id } });
                if (!user) return false;

                if (!roles.includes(user.userType)) return false;

                return true;
            } catch (err) { return false; };
        }
        return true;
    }

    private verifyToken(token: string) {
        return jwt.verify(token, process.env.JWT_SECRET) as JWTPayload;
    }
}