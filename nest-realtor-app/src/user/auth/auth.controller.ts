import { Body, Controller, Param, ParseEnumPipe, Post } from '@nestjs/common';
import { HttpException } from '@nestjs/common/exceptions';
import * as bcrypt from 'bcryptjs';
import { UserType } from '@prisma/client';
import { AuthService } from './auth.service';
import { GenerateRegistrationKeyDto, RegisterSuperAdminDto, SigninDto, SignupDto } from './dtos/auth.controller.dto';
import { Roles } from './decorators/auth.decorator';

@Controller('auth')
export class AuthController {
    constructor(private readonly authService: AuthService) { }

    @Post('/register/superadmin')
    async registerSuperAdmin(
        @Body() body: RegisterSuperAdminDto
    ) {
        if (body.verificationPassword !== process.env.SUPER_ADMIN_PASSWORD)
            throw new HttpException('Invalid Credentials', 400);

        return this.authService.registerSuperAdmin({
            firstName: body.firstName,
            lastName: body.lastName,
            email: body.email,
            password: body.accountPassword,
            phone: body.phone
        });
    }

    @Post('/signup/:userType')
    async signup(
        @Param('userType', new ParseEnumPipe(UserType)) userType: UserType,
        @Body() body: SignupDto
    ) {
        if (userType !== UserType.BUYER) {
            if (!body.registrationKey)
                throw new HttpException("Registration Key required", 400);

            const isValid = await bcrypt.compare(
                `${userType}-${body.email}-${process.env.KEY_SECRET}`,
                body.registrationKey
            );

            if (!isValid)
                throw new HttpException("Invalid Credentials", 400);
        }
        return this.authService.signup(body, userType);
    }

    @Post('/signin')
    signin(
        @Body() body: SigninDto
    ) {
        return this.authService.signin(body);
    }

    @Roles(UserType.SUPERADMIN, UserType.ADMIN)
    @Post('/key')
    async generateRegistrationKey(
        @Body() { email, userType }: GenerateRegistrationKeyDto
    ) {
        return this.authService.generateRegistrationKey(email, userType);
    }
}
