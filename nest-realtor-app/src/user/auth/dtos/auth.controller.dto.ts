import { IsString, IsOptional, IsEmail, IsNotEmpty, Matches, MinLength, IsEnum, IsJWT } from 'class-validator';
import { UserType } from '@prisma/client';
import { UserResponseDto } from '../../../user/dtos/user.controller.dto';

export class AuthResponseDto {
    user: UserResponseDto;
    token: string;

    constructor(obj: AuthResponseDto) {
        Object.assign(this, obj);
    }
}

export class RegisterSuperAdminDto {

    @IsString()
    @IsNotEmpty()
    firstName: string;

    @IsString()
    @IsNotEmpty()
    lastName: string;

    @IsEmail()
    email: string;

    @IsString()
    @MinLength(8)
    accountPassword: string;

    @IsString()
    @IsNotEmpty()
    verificationPassword: string;

    @Matches(/^\s*(?:\+?(\d{1,3}))?[-. (]*(\d{3})[-. )]*(\d{3})[-. ]*(\d{4})(?: *x(\d+))?\s*$/)
    phone: string;
}

export class SignupDto {
    @IsString()
    @IsNotEmpty()
    firstName: string;

    @IsString()
    @IsNotEmpty()
    lastName: string;

    @IsEmail()
    email: string;

    @Matches(/^\s*(?:\+?(\d{1,3}))?[-. (]*(\d{3})[-. )]*(\d{3})[-. ]*(\d{4})(?: *x(\d+))?\s*$/)
    phone: string;

    @IsString()
    @MinLength(7)
    password: string;

    @IsOptional()
    @IsString()
    @IsNotEmpty()
    registrationKey?: string;
}

export class SigninDto {
    @IsEmail()
    email: string;

    @IsString()
    @MinLength(7)
    password: string;
}

export class GenerateRegistrationKeyDto {
    @IsEmail()
    email: string;

    @IsEnum(UserType)
    userType: UserType;
}

export type RegisterSuperAdmin = {
    firstName: string;
    lastName: string;
    email: string;
    password: string;
    phone: string;
};

export type Signup = {
    firstName: string;
    lastName: string;
    email: string;
    password: string;
    phone: string;
    registrationKey?: string;
};

export type Signin = {
    email: string;
    password: string;
};

export type JWTPayload = {
    id: string;
};