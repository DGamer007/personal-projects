import { Injectable, OnModuleDestroy, OnModuleInit } from '@nestjs/common';
import { PrismaClient } from '@prisma/client';


@Injectable()
export class PrismaService extends PrismaClient implements OnModuleInit, OnModuleDestroy {

    constructor() {
        super({
            datasources: {
                db: {
                    url: process.env.DATABASE_URL
                }
            }
        });
    }

    async onModuleInit() {
        this.$connect;
    }

    async onModuleDestroy() {
        this.$disconnect;
    }
}

@Injectable()
export class TestPrismaService extends PrismaClient implements OnModuleInit, OnModuleDestroy {

    constructor() {
        super({
            datasources: {
                db: {
                    url: process.env.TEST_DATABASE_URL
                }
            }
        });
    }

    async onModuleInit() {
        this.$connect;
    }

    async onModuleDestroy() {
        this.$disconnect;
    }
}