import { UserType } from '@prisma/client';
import { generateRegistrationKey, hashPassword } from './__utils__';

const fakeUsers = {
    buyer: {
        uniqueValue: { email: 'somebuyer@something.com' },
        info: {
            firstName: 'Some',
            lastName: 'Buyer',
            email: 'somebuyer@something.com',
            password: hashPassword('somebuyer@something'),
            phone: '123-456-7890',
            userType: UserType.BUYER
        }
    },
    admin: {
        uniqueValue: { email: 'admin@something.com' },
        info: {
            firstName: 'Some',
            lastName: 'Admin',
            email: 'admin@something.com',
            password: hashPassword('admin@something'),
            userType: UserType.ADMIN,
            phone: '123-456-7890'
        }
    },
    superAdmin: {
        uniqueValue: { email: 'superadmin@something.com' },
        info: {
            firstName: 'Super',
            lastName: 'Admin',
            email: 'superadmin@something.com',
            password: hashPassword('superadmin@something'),
            userType: UserType.SUPERADMIN,
            phone: '123-456-7890'
        },
    }
};

export const testData = {
    '/auth/register/superadmin': {
        uniqueValue: { email: 'superadmin@something.com' },
        test1: {
            req: [
                // Incorrect
                {
                    firstName: 'dgamer',
                    lastName: '007',
                    email: 'superadmin@something.com',
                    accountPassword: 'super', // Incorrect
                    verificationPassword: 'wrong-verification-password',
                    phone: '123-456-7890'
                },
                // Incomplete
                {
                    firstName: 'dgamer',
                    lastName: '007',
                    email: 'superadmin@something.com',
                    accountPassword: 'superadmin@something',
                    verificationPassword: 'wrong-verification-password',
                    phone: '123-456-7890'
                }
            ]
        },
        test2: {
            req: {
                firstName: 'dgamer',
                lastName: '007',
                email: 'superadmin@something.com',
                accountPassword: 'superadmin@something',
                verificationPassword: 'wrong-verification-password',
                phone: '123-456-7890'
            }
        },
        test3: {
            req: {
                firstName: 'dgamer',
                lastName: '007',
                email: 'superadmin@something.com',
                accountPassword: 'superadmin@something',
                verificationPassword: process.env.SUPER_ADMIN_PASSWORD,
                phone: '123-456-7890'
            }
        }
    },
    '/auth/signup/BUYER': {
        uniqueValue: { email: 'user1@something.com' },
        test1: {
            req: [
                // Incorrect
                {
                    firstName: 'user1',
                    lastName: 'somefamily',
                    email: 'user1@something', // Incorrect
                    password: 'user1@something',
                    phone: '123-456-7890'
                },
                // Incomplete
                {
                    firstName: 'user1',
                    lastName: 'somefamily',
                    email: 'user1@something.com',
                    password: 'user1@something'
                }
            ]
        },
        test2: {
            req: {
                firstName: 'user1',
                lastName: 'somefamily',
                email: 'superadmin@someplace.com',
                password: 'user1@something',
                phone: '123-456-7890'
            }
        },
        test3: {
            req: {
                firstName: 'user1',
                lastName: 'somefamily',
                email: 'user1@something.com',
                password: 'user1@something',
                phone: '123-456-7890'
            }
        },
    },
    '/auth/signup/REALTOR': {
        uniqueValue: { email: 'user2@something.com' },
        test1: {
            req: [
                // Incorrect Data
                {
                    firstName: 'user2',
                    lastName: 'somefamily',
                    email: 'user2@something.com',
                    password: 'user2@something',
                    phone: '123-456-789', // Incorrect
                    registrationKey: generateRegistrationKey('user2@something.com', 'REALTOR')
                },
                // Incomplete Data
                {
                    firstName: 'user2',
                    lastName: 'somefamily',
                    email: 'user2@something.com',
                    password: 'user2@something',
                    registrationKey: generateRegistrationKey('user2@something.com', 'REALTOR')
                }
            ]
        },
        test2: {
            req: {
                firstName: 'user2',
                lastName: 'somefamily',
                email: 'user2@something.com',
                password: 'user2@something',
                phone: '123-456-7890'
            }
        },
        test3: {
            req: {
                firstName: 'user2',
                lastName: 'somefamily',
                email: 'user2@something.com',
                password: 'user2@something',
                phone: '123-456-7890',
                registrationKey: 'somefakeregistration'
            }
        },
        test4: {
            req: {
                firstName: 'user2',
                lastName: 'somefamily',
                email: 'superadmin@blahblah.com',
                password: 'user2@something',
                phone: '123-456-7890',
                registrationKey: generateRegistrationKey('superadmin@blahblah.com', 'REALTOR')
            }
        },
        test5: {
            req: {
                firstName: 'user2',
                lastName: 'somefamily',
                email: 'user2@something.com',
                password: 'user2@something',
                phone: '123-456-7890',
                registrationKey: generateRegistrationKey('user2@something.com', 'REALTOR')
            }
        }
    },
    '/auth/signup/ADMIN': {
        uniqueValue: { email: 'admin@something.com' },
        test1: {
            req: [
                // Incorrect Data
                {
                    firstName: 'John',
                    lastName: 'Wick',
                    email: 'admin@something.com',
                    password: 'admin@something',
                    phone: '123-456-789', // Incorrect
                    registrationKey: generateRegistrationKey('user2@something.com', 'ADMIN')
                },
                // Incomplete Data
                {
                    firstName: 'John',
                    lastName: 'Wick',
                    email: 'admin@something.com',
                    password: 'admin@something',
                    registrationKey: generateRegistrationKey('user2@something.com', 'ADMIN')
                }
            ]
        },
        test2: {
            req: {
                firstName: 'John',
                lastName: 'Wick',
                email: 'admin@something.com',
                password: 'admin@something',
                phone: '123-456-7890'
            }
        },
        test3: {
            req: {
                firstName: 'John',
                lastName: 'Wick',
                email: 'admin@something.com',
                password: 'admin@something',
                phone: '123-456-7890',
                registrationKey: 'somefakeregistration'
            }
        },
        test4: {
            req: {
                firstName: 'John',
                lastName: 'Wick',
                email: 'superadmin@blahblah.com',
                password: 'admin@something',
                phone: '123-456-7890',
                registrationKey: generateRegistrationKey('superadmin@blahblah.com', 'ADMIN')
            }
        },
        test5: {
            req: {
                firstName: 'John',
                lastName: 'Wick',
                email: 'admin@something.com',
                password: 'admin@something',
                phone: '123-456-7890',
                registrationKey: generateRegistrationKey('admin@something.com', 'ADMIN')
            }
        }
    },
    '/auth/signin': {
        uniqueValue: { email: 'dgamer@something.com' },
        user: {
            firstName: 'user1',
            lastName: 'somefamily',
            email: 'dgamer@something.com',
            password: hashPassword('dgamer@something'),
            phone: '123-456-7890',
            userType: UserType.BUYER
        },
        test1: {
            req: [
                // Incorrect
                {
                    email: 'dgamer@something',  // Incorrect
                    password: 'dgamer@something'
                },
                // Incomplete
                {
                    email: 'dgamer@something.com'
                }
            ]
        },
        test2: {
            req: {
                email: 'unknown@something.com',
                password: 'unknown@something'
            }
        },
        test3: {
            req: [
                {
                    email: 'dgamer@something.com',
                    password: 'dgamer@blahblah'
                },
                {
                    email: 'dgamer@something.com',
                    password: 'dgamer@something'
                }
            ]
        }
    },
    '/auth/key':
    {
        test1: {
            req: {
                email: 'someone@something.com',
                userType: UserType.ADMIN
            }
        },
        test2: {
            uniqueValue: fakeUsers.buyer.uniqueValue,
            user: fakeUsers.buyer.info,
            req: {
                email: 'somebuyer@something.com',
                password: 'somebuyer@something'
            }
        },
        test3: [
            {
                uniqueValue: fakeUsers.admin.uniqueValue,
                user: fakeUsers.admin.info,
                req: [
                    [
                        // Incomplete
                        {
                            email: 'somerealtor@something.com'
                        },
                        // Incorrect
                        {
                            email: 'somerealtor@something', // Incorrect
                            userType: UserType.ADMIN
                        }
                    ],
                    {
                        email: 'somerealtor@something.com',
                        userType: UserType.REALTOR
                    }
                ]
            },
            {
                uniqueValue: fakeUsers.superAdmin.uniqueValue,
                user: fakeUsers.superAdmin.info,
                req: [
                    [
                        // Incomplete
                        {
                            email: 'someadmin@something.com'
                        },
                        // Incorrect
                        {
                            email: 'someadmin@something', // Incorrect
                            userType: UserType.REALTOR
                        }
                    ],
                    {
                        email: 'someadmin@something.com',
                        userType: UserType.ADMIN
                    }
                ]
            }
        ]
    }
};