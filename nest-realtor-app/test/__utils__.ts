import * as JWT from 'jsonwebtoken';
import * as bcrypt from 'bcryptjs';

export const generateRegistrationKey = (email: string, userType: string) => {
    return bcrypt.hashSync(`${userType}-${email}-${process.env.KEY_SECRET}`, 10);
};

export const generateAuthToken = (id: string) => {
    return JWT.sign({ id }, process.env.JWT_SECRET);
};

export const hashPassword = (password: string) => {
    return bcrypt.hashSync(password, 10);
};