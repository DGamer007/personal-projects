import * as request from 'supertest';
import { INestApplication, ValidationPipe } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { AppModule } from '../../src/app.module';
import { PrismaService, TestPrismaService } from '../../src/prisma/prisma.service';
import { testData } from '../__mock__';
import { generateAuthToken } from '../__utils__';

describe('AuthController (e2e)', () => {

    let app: INestApplication;
    let prisma: TestPrismaService;

    const AuthResponseExpected = {
        token: expect.any(String),
        user: expect.objectContaining({
            id: expect.any(String),
            firstName: expect.any(String),
            lastName: expect.any(String),
            email: expect.any(String),
            phone: expect.any(String)
        })
    };

    beforeAll(async () => {
        const module: TestingModule = await Test.createTestingModule({
            imports: [AppModule],
            providers: [PrismaService]
        })
            .overrideProvider(PrismaService)
            .useClass(TestPrismaService)
            .compile();


        prisma ??= module.get<PrismaService>(PrismaService);

        app = module.createNestApplication();
        app.useGlobalPipes(
            new ValidationPipe({
                whitelist: true,
                transform: true,
                transformOptions: { enableImplicitConversion: true }
            })
        );
        await app.init();
    });

    describe('Register Super Admin', () => {
        const route = '/auth/register/superadmin';

        it.each([
            testData[route].test1[0],
            testData[route].test1[1],
            null
        ])('Test1 - With Incomplete/Invalid Data', (data) => {
            return request(app.getHttpServer())
                .post(route)
                .send(data)
                .expect(400);
        });

        it('Test2 - With wrong VerificationPassword', () => {
            return request(app.getHttpServer())
                .post(route)
                .send(testData[route].test2.req)
                .expect(400)
                .expect({
                    statusCode: 400,
                    message: 'Invalid Credentials'
                });
        });

        describe('Test3', () => {
            it('- With correct Data', async () => {
                const response = await request(app.getHttpServer())
                    .post(route)
                    .send(testData[route].test3.req);

                expect(response.statusCode).toBe(201);
                expect(response.body).toEqual(AuthResponseExpected);

            });

            it('- SUPERADMIN already exists', () => {
                return request(app.getHttpServer())
                    .post(route)
                    .send(testData[route].test3.req)
                    .expect(409)
                    .expect({
                        statusCode: 409,
                        message: 'Conflict'
                    });
            });

            afterAll(async () => {
                const user = await prisma.user.findUnique({ where: testData[route].uniqueValue });
                if (user)
                    await prisma.user.delete({ where: testData[route].uniqueValue });
            });

        });
    });

    describe('Signup Buyer', () => {
        const route = '/auth/signup/BUYER';

        it.each([
            testData[route].test1.req[0],
            testData[route].test1.req[1],
            null
        ])('Test1 - With Incomplete/Invalid Data', (data) => {
            return request(app.getHttpServer())
                .post(route)
                .send(data)
                .expect(400);
        });

        it('Test2 - Email Not Acceptable', () => {
            return request(app.getHttpServer())
                .post(route)
                .send(testData[route].test2.req)
                .expect(406)
                .expect({
                    statusCode: 406,
                    message: 'Not Acceptable'
                });
        });

        describe('Test3', () => {
            it('- With correct Data', async () => {
                const response = await request(app.getHttpServer())
                    .post(route)
                    .send(testData[route].test3.req);

                expect(response.statusCode).toBe(201);
                expect(response.body).toEqual(AuthResponseExpected);
            });

            it('- Buyer with same Email already exists', () => {
                return request(app.getHttpServer())
                    .post(route)
                    .send(testData[route].test3.req)
                    .expect(409)
                    .expect({
                        statusCode: 409,
                        message: 'Conflict'
                    });
            });

            afterAll(async () => {
                const user = await prisma.user.findUnique({ where: testData[route].uniqueValue });
                if (user)
                    await prisma.user.delete({ where: testData[route].uniqueValue });
            });
        });
    });

    describe.each([
        '/auth/signup/REALTOR',
        '/auth/signup/ADMIN'
    ])('Signup Realtor & Signup Admin', (route) => {

        it.each([
            testData[route].test1.req[0],
            testData[route].test1.req[1],
            null
        ])('Test1 - With Incorrect/Incomplete Data', (data) => {
            return request(app.getHttpServer())
                .post(route)
                .send(data)
                .expect(400);
        });

        it('Test2 - Without providing RegistrationKey', () => {
            return request(app.getHttpServer())
                .post(route)
                .send(testData[route].test2.req)
                .expect(400)
                .expect({
                    statusCode: 400,
                    message: 'Registration Key required'
                });
        });

        it('Test3 - Wrong Registration Key', () => {
            return request(app.getHttpServer())
                .post(route)
                .send(testData[route].test3.req)
                .expect(400)
                .expect({
                    statusCode: 400,
                    message: 'Invalid Credentials'
                });
        });

        it('Test4 - Email Not Acceptable', async () => {
            return request(app.getHttpServer())
                .post(route)
                .send(testData[route].test4.req)
                .expect(406)
                .expect({
                    statusCode: 406,
                    message: 'Not Acceptable'
                });
        });

        describe('Test5', () => {
            it('- With correct Data', async () => {
                const response = await request(app.getHttpServer())
                    .post(route)
                    .send(testData[route].test5.req);

                expect(response.statusCode).toBe(201);
                expect(response.body).toEqual(AuthResponseExpected);
            });

            it('- Realtor with same Email already exists', () => {
                return request(app.getHttpServer())
                    .post(route)
                    .send(testData[route].test5.req)
                    .expect(409)
                    .expect({
                        statusCode: 409,
                        message: 'Conflict'
                    });
            });

            afterAll(async () => {
                const user = await prisma.user.findUnique({ where: testData[route].uniqueValue });
                if (user)
                    await prisma.user.delete({ where: testData[route].uniqueValue });
            });
        });
    });

    describe('Signin User', () => {
        const route = '/auth/signin';

        it.each([
            testData[route].test1.req[0],
            testData[route].test1.req[1],
            null
        ])('Test1 - With Incomplete/Incorrect Data', (data) => {
            return request(app.getHttpServer())
                .post(route)
                .send(data)
                .expect(400);
        });

        it('Test2 - User does not exist', () => {
            return request(app.getHttpServer())
                .post(route)
                .send(testData[route].test2.req)
                .expect(400)
                .expect({
                    statusCode: 400,
                    message: 'Invalid Credentials'
                });
        });

        describe('Test3', () => {

            beforeAll(async () => {
                await prisma.user.create({
                    data: testData[route].user
                });
            });

            it('- With Wrong Password', () => {
                return request(app.getHttpServer())
                    .post(route)
                    .send(testData[route].test3.req[0])
                    .expect(400)
                    .expect({
                        statusCode: 400,
                        message: 'Invalid Credentials'
                    });
            });

            it('- With correct Data', async () => {
                const response = await request(app.getHttpServer())
                    .post(route)
                    .send(testData[route].test3.req[1]);

                expect(response.body).toEqual(AuthResponseExpected);
            });

            afterAll(async () => {
                const user = await prisma.user.findUnique({ where: testData[route].uniqueValue });
                if (user)
                    await prisma.user.delete({ where: testData[route].uniqueValue });
            });
        });
    });

    describe('Generate Registration Key', () => {
        const route = '/auth/key';

        it('Test1 - Without Authorization Token', () => {
            return request(app.getHttpServer())
                .post(route)
                .send(testData[route].test1.req)
                .expect(403);
        });

        describe('', () => {
            let token = '';
            beforeAll(async () => {
                const user = await prisma.user.create({ data: testData[route].test2.user });
                token = generateAuthToken(user.id);
            });

            it('Test2 - Generating token Using BUYER Account', () => {
                return request(app.getHttpServer())
                    .post(route)
                    .set('Authorization', `Bearer ${token}`)
                    .send(testData[route].test2.req)
                    .expect(403);
            });

            afterAll(async () => {
                const user = await prisma.user.findUnique({ where: testData[route].test2.uniqueValue });
                if (user)
                    await prisma.user.delete({ where: testData[route].test2.uniqueValue });
            });
        });

        describe.each(testData[route].test3)('Test3', (role) => {
            let token = '';
            beforeAll(async () => {
                const user = await prisma.user.create({ data: role.user });
                token = generateAuthToken(user.id);
            });

            it.each([
                role.req[0][0],
                role.req[0][1],
                null
            ]
            )('- With Incomplete/Incorrect Data', (data) => {
                return request(app.getHttpServer())
                    .post(route)
                    .set('Authorization', `Bearer ${token}`)
                    .send(data)
                    .expect(400);
            });

            it('- With Correct Data', async () => {
                const response = await request(app.getHttpServer())
                    .post(route)
                    .set('Authorization', `Bearer ${token}`)
                    .send(role.req[1]);

                expect(response.status).toBe(201);
                expect(response.body).toEqual({
                    registrationKey: expect.any(String)
                });
            });

            afterAll(async () => {
                const user = await prisma.user.findUnique({ where: role.uniqueValue });
                if (user)
                    await prisma.user.delete({ where: role.uniqueValue });
            });
        });
    });

    afterAll(async () => {
        await app.close();
    });
});