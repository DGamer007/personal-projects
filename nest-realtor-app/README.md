# Nest-Realtor-App

A Basic Nest.js CRUD Application for users to buy and sell their houses.

## Overview

This Nest.js Application contains Restful-APIs for performing CRUD operations, necessary for achieving the goal of this Project which is to *Buy and Sell houses*. This Application also utilizes **RBAC** capabilities of Nest.js. It uses **Prisma** as ORM to connect with **PostgreSQL**. Along with that, it has E2E test cases implemented to automate the testing of Rest APIs.

#### Contributors:

> [Dhruv Prajapati](https://github.com/DGamer007)


