const { database } = require('./firebase')
const uuid = require('uuid')
const moment = require('moment')
const fs = require('fs')
const path = require('path')

const addDefaultData = async () => {
    try {
        const data = JSON.parse(fs.readFileSync(path.join(__dirname, 'staticData.json')).toString())

        for (let i in data) {
            await database.ref(`/system/${uuid()}`).set({
                productName: data[i].product_name,
                amount: parseInt((Math.random() * (5000 - 500 + 1)) + 500),
                subCategory: data[i].aisle,
                category: data[i].department
            })
        }

        console.log('Added Default Data Successfully!')
    } catch (error) {
        console.log(error)
    }
}

const addCartData = async (uid, yearLimit, monthLimit) => {
    let j = 0;
    let k = j + parseInt(Math.random() * (50 - 10) + 10)

    let year = 2015
    let month = 1
    let day = parseInt(Math.random() * (17 - 5) + 5)
    let date = moment(`${year}-${month}-${day}`, 'YYYY-M-D').format('YYYY-MM-DD')

    let data = await (await database.ref('/system').limitToFirst(k).once('value')).val()

    while (year !== yearLimit || month !== monthLimit) {
        day = parseInt(Math.random() * (17 - 5) + 5)
        date = moment(`${year}-${month}-${day}`, 'YYYY-M-D').format('YYYY-MM-DD')

        for (let product in data) {
            const { productName, amount, category, subCategory } = data[product]

            await database.ref(`users/${uid}/history/${date}/${product}`)
                .set({
                    productName,
                    amount,
                    category,
                    subCategory
                })
        }

        j = k;
        k = j + parseInt(Math.random() * (50 - 10) + 10)

        data = await (await database.ref('/system')
            .orderByKey()
            .startAfter(Object.keys(data).pop())
            .limitToFirst(k - j)
            .once('value'))
            .val()

        if (month == 12) {
            month = 1
            year++
        } else {
            month++
        }
    }

    console.log('Added Cart Data Successfully!')
}

(async () => {
    // await addDefaultData()
    await addCartData('kQea4pm6dnhMhBLRNE0khSQ5BtI2', 2021, 8)

    console.log('All Done')
})()