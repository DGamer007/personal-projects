# Smart Bag

A complete React-JS Project which will allow users to buy products of their choice also users can use the feature called **Smart Bag**.

## Overview

Smart Bag is a Web-Application made from **ReactJS** (Frontend framework), **NodeJS** (Backend framework), **firebase** (Cloud Database).

In this Web-Application users can purchase products _(the buying feature is not real but it'll save those products in database, so eventually you won't get any product in real)_.

Also, this project is using **firebase-Authentication** System. Now, I have mentioned the **Smart Bag** feature in above section and it is...

`A feature which will show best products to the user depending upon his previous purchase history so that a user can buy products easily that he has been buying since long and to get those best products we are using a ` **Machine Learning Model** ` to process user's purchase history.`

If you wanna take a look at ML Model Source Code [Click Here](https://github.com/smit977/Smart-Bag).

#### Contributors:

> [Dhruv Prajapati](https://github.com/DGamer007)

> [Smit Prajapati](https://github.com/smit977)
