import moment from 'moment'
import getVisibleExpenses, { getExpensesAmountTotal } from '../../src/selectors/expenses'
import expenses from '../fixtures/expenses'

test('Should filter data by Text', () => {
    const filters = {
        text: 'i',
        sortBy: '',
        startDate: undefined,
        endDate: undefined
    }

    const result = getVisibleExpenses(expenses, filters)

    expect(result).toEqual([expenses[0], expenses[2]])
})

test('Should filter data by Start Date', () => {
    const filters = {
        text: '',
        sortBy: '',
        startDate: moment(0),
        endDate: undefined
    }

    const result = getVisibleExpenses(expenses, filters)

    expect(result).toEqual([expenses[0], expenses[2]])
})

test('Should filter data by End Date', () => {
    const filters = {
        text: '',
        sortBy: '',
        startDate: undefined,
        endDate: moment(0)
    }

    const result = getVisibleExpenses(expenses, filters)

    expect(result).toEqual([expenses[0], expenses[1]])
})

test('Should sort data by Date', () => {
    const filters = {
        text: '',
        sortBy: 'date',
        startDate: undefined,
        endDate: undefined
    }

    const result = getVisibleExpenses(expenses, filters)

    expect(result).toEqual([expenses[2], expenses[0], expenses[1]])
})

test('Should sort data by Amount', () => {
    const filters = {
        text: '',
        sortBy: 'amount',
        startDate: undefined,
        endDate: undefined
    }

    const result = getVisibleExpenses(expenses, filters)

    expect(result).toEqual([expenses[1], expenses[2], expenses[0]])
})

test('Should return 0 for no Expenses', () => {
    const result = getExpensesAmountTotal([])
    expect(result).toBe(0)
})

test('Should work for single Expense', () => {
    const result = getExpensesAmountTotal([expenses[0]])
    expect(result).toBe(expenses[0].amount)
})

test('Should work for multiple Expenses', () => {
    const result = getExpensesAmountTotal(expenses)
    expect(result).toBe(950)
})