import React from 'react'
import { shallow } from 'enzyme'
import { ExpensesSummary } from '../../src/components/ExpensesSummary'
import expenses from '../fixtures/expenses'
import { expect } from '@jest/globals'

test('Should render ExpensesSummary with 1 expense', () => {
    const wrapper = shallow(<ExpensesSummary expenseCount={1} expenseTotal={950} />)
    expect(wrapper).toMatchSnapshot()
})

test('Should render ExpensesSummary with multiple expenses', () => {
    const wrapper = shallow(<ExpensesSummary expenseCount={3} expenseTotal={2000} />)
    expect(wrapper).toMatchSnapshot()
})
