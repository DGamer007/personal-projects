import React from 'react'
import { LoginPage } from '../../src/components/LoginPage'
import { shallow } from 'enzyme'

test('Should render LoginPage correctly', () => {
    const wrapper = shallow(<LoginPage />)

    expect(wrapper).toMatchSnapshot()
})

test('Should handle OnClick', () => {
    const startLogin = jest.fn()
    const wrapper = shallow(<LoginPage startLogin={startLogin} />)

    wrapper.find('button').simulate('click')
    expect(startLogin).toHaveBeenCalled()
})