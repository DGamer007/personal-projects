import React from 'react'
import { shallow } from 'enzyme'
import { ExpenseListFilter } from '../../src/components/ExpenseListFilter'
import filters, { altFilters } from '../fixtures/filters'
import 'react-dates/initialize'
import { DateRangePicker } from 'react-dates'
import { expect, jest } from '@jest/globals'

let setTextFilter, sortByDate, sortByAmount, setStartDate, setEndDate, wrapper

beforeEach(() => {
    setTextFilter = jest.fn()
    sortByDate = jest.fn()
    sortByAmount = jest.fn()
    setStartDate = jest.fn()
    setEndDate = jest.fn()

    wrapper = shallow(<ExpenseListFilter
        filters={filters}
        setTextFilter={setTextFilter}
        sortByAmount={sortByAmount}
        sortByDate={sortByDate}
        setStartDate={setStartDate}
        setEndDate={setEndDate}
    />)
})

test('Should render ExpenseListFilter', () => {
    expect(wrapper).toMatchSnapshot()
})

test('Should render ExpenseListFilter with altFilters', () => {
    wrapper.setProps({ filters: altFilters })
    expect(wrapper).toMatchSnapshot()
})

test('Should handle Text Filters', () => {
    const value = 'someText'
    wrapper.find('input').simulate('change', {
        target: {
            value
        }
    })
    expect(setTextFilter).toHaveBeenLastCalledWith(value)
})

test('Should handle Sort By Date Filter', () => {
    wrapper.find('select').simulate('change', {
        target: {
            value: 'date'
        }
    })

    expect(sortByDate).toHaveBeenCalled()
})

test('Should handle Sort By Amount Filter', () => {
    wrapper.find('select').simulate('change', {
        target: {
            value: 'amount'
        }
    })

    expect(sortByAmount).toHaveBeenCalled()
})

test('Should handle Dates Change Filter', () => {
    const startDate = altFilters.startDate
    const endDate = altFilters.endDate
    wrapper.find(DateRangePicker).prop('onDatesChange')({ startDate, endDate })

    expect(setStartDate).toHaveBeenLastCalledWith(startDate)
    expect(setEndDate).toHaveBeenLastCalledWith(endDate)
})

test('Should handle Focus Change', () => {
    const calenderFocused = 'endDate'
    wrapper.find(DateRangePicker).prop('onFocusChange')(calenderFocused)

    expect(wrapper.state('calenderFocused')).toBe(calenderFocused)
})