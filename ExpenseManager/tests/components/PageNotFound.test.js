import React from 'react'
import { shallow } from 'enzyme'
import PageNotFound from '../../src/components/PageNotFound'

test('Should render PageNotFound',()=>{
    const wrapper = shallow(<PageNotFound/>)

    expect(wrapper).toMatchSnapshot()
})