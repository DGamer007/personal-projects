import React from 'react'
import { shallow } from 'enzyme'
import { EditExpense } from '../../src/components/EditExpense'
import expenses from '../fixtures/expenses'
import { beforeEach, expect, jest } from '@jest/globals'
import ExpenseForm from '../../src/components/ExpenseForm'

let startEditExpense, startRemoveExpense, history, wrapper, expense

beforeEach(() => {
    startEditExpense = jest.fn()
    startRemoveExpense = jest.fn()
    history = {
        push: jest.fn()
    }
    expense = expenses[0]

    wrapper = shallow(<EditExpense startEditExpense={startEditExpense} history={history} startRemoveExpense={startRemoveExpense} expense={expense} />)
})

test('Should render EditExpense', () => {
    expect(wrapper).toMatchSnapshot()
})

test('Should handle onSubmit', () => {
    wrapper.find(ExpenseForm).prop('onSubmit')(expense)
    expect(startEditExpense).toHaveBeenLastCalledWith(expense.id, expense)
    expect(history.push).toHaveBeenLastCalledWith('/')
})

test('Should handle onClick', () => {
    wrapper.find('button').simulate('click')
    expect(startRemoveExpense).toHaveBeenLastCalledWith(expense.id)
    expect(history.push).toHaveBeenLastCalledWith('/')
})