import React from 'react'
import { shallow } from 'enzyme'
import ExpenseDashboard from '../../src/components/ExpenseDashboard'
import { expect } from '@jest/globals'

test('Should render ExpenseDashboard',()=>{
    const wrapper = shallow(<ExpenseDashboard />)
    expect(wrapper).toMatchSnapshot()
})