import React from 'react'
import { shallow } from 'enzyme'
import { AddExpense } from '../../src/components/AddExpense'
import expenses from '../fixtures/expenses'
import ExpenseForm from '../../src/components/ExpenseForm'
import { beforeEach, expect, jest } from '@jest/globals'

let startAddExpense, history, wrapper

beforeEach(() => {
    startAddExpense = jest.fn()
    history = {
        push: jest.fn()
    }
    wrapper = shallow(<AddExpense startAddExpense={startAddExpense} history={history} />)
})

test('Should render AddExpense', () => {
    expect(wrapper).toMatchSnapshot()
})

test('Should handle onSubmit', () => {
    wrapper.find(ExpenseForm).prop('onSubmit')(expenses[0])
    expect(startAddExpense).toHaveBeenLastCalledWith(expenses[0])
    expect(history.push).toHaveBeenLastCalledWith('/')
})