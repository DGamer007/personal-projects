import { expect } from '@jest/globals'
import { shallow } from 'enzyme'
import toJSON from 'enzyme-to-json'
import React from 'react'
import { Header } from '../../src/components/Header'

let wrapper, startLogout

beforeEach(() => {
    startLogout = jest.fn()
    wrapper = shallow(<Header startLogout={startLogout} />)
})

test('Should render Header correctly', () => {
    expect(toJSON(wrapper)).toMatchSnapshot()
})

test('Should handle Onclick', () => {
    wrapper.find('button').simulate('click')

    expect(startLogout).toHaveBeenCalled()
})