import React from 'react'
import { shallow } from 'enzyme'
import { ExpenseList } from "../../src/components/ExpenseList"
import expenses from '../fixtures/expenses'
import { expect } from '@jest/globals'

test('Should render ExpenseList with Expenses',()=>{
    const wrapper = shallow(<ExpenseList expenses={expenses} />)

    expect(wrapper).toMatchSnapshot()
})

test('Should render ExpenseList with 0 Expenses',()=>{
    const wrapper = shallow(<ExpenseList expenses={[]}/>)

    expect(wrapper).toMatchSnapshot()
})

