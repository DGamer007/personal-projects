import React from 'react'
import { shallow } from 'enzyme'
import ExpenseForm from '../../src/components/ExpenseForm'
import { expect } from '@jest/globals'
import expenses from '../fixtures/expenses'
import moment from 'moment'
import 'react-dates/initialize'
import { SingleDatePicker } from 'react-dates'

test('Should render ExpenseForm correctly', () => {
    const wrapper = shallow(<ExpenseForm />)

    expect(wrapper).toMatchSnapshot()
})

test('Should render ExpenseForm with Data', () => {
    const wrapper = shallow(<ExpenseForm expense={expenses[0]} />)

    expect(wrapper).toMatchSnapshot()
})

test('Should render Error for invalid form submission', () => {
    const wrapper = shallow(<ExpenseForm />)

    wrapper.find('form').simulate('submit', {
        preventDefault() { }
    })

    expect(wrapper.state('error').length).toBeGreaterThan(0)
    expect(wrapper).toMatchSnapshot()
})

test('Should set description on Input Change', () => {
    const value = 'someDescription'
    const wrapper = shallow(<ExpenseForm />)
    wrapper.find('input').at(0).simulate('change', {
        target: { value }
    })

    expect(wrapper.state('description')).toBe(value)
})

test('Should set note on TextArea Change', () => {
    const value = 'something'
    const wrapper = shallow(<ExpenseForm />)

    wrapper.find('textarea').simulate('change', {
        target: { value }
    })

    expect(wrapper.state('note')).toBe(value)
})

test('Should set amount on valid Input', () => {
    const value = '23.50'
    const wrapper = shallow(<ExpenseForm />)

    wrapper.find('input').at(1).simulate('change', {
        target: { value }
    })

    expect(wrapper.state('amount')).toBe(value)
})

test('Should not set amount on invalid Input', () => {
    const value = '12.122'
    const wrapper = shallow(<ExpenseForm />)

    wrapper.find('input').at(1).simulate('change', {
        target: { value }
    })

    expect(wrapper.state('amount')).not.toBe(value)
})

test('Should submit data', () => {
    const onSubmitSpy = jest.fn()
    const wrapper = shallow(<ExpenseForm expense={expenses[0]} onSubmit={onSubmitSpy} />)

    wrapper.find('form').simulate('submit', {
        preventDefault() { }
    })

    expect(wrapper.state('error')).toBe('')

    expect(onSubmitSpy).toHaveBeenCalledWith({
        description: expenses[0].description,
        amount: expenses[0].amount,
        note: expenses[0].note,
        createdAt: expenses[0].createdAt
    })
})

test('Should change date', () => {
    const createdAt = moment()
    const wrapper = shallow(<ExpenseForm />)

    wrapper.find(SingleDatePicker).prop('onDateChange')(createdAt)
    expect(wrapper.state('createdAt')).toEqual(createdAt)
})

test('Should change focus', () => {
    const focused = true
    const wrapper = shallow(<ExpenseForm />)

    wrapper.find(SingleDatePicker).prop('onFocusChange')({ focused })

    expect(wrapper.state('calenderFocused')).toBe(focused)
})