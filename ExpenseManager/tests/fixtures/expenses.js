import moment from 'moment'

const expenses = [
    {
        id:'1',
        description: 'Morning Coffee',
        amount:200,
        note:'',
        createdAt:0
    },
    {
        id:'2',
        description: 'Lunch',
        amount:400,
        note:'',
        createdAt:moment(0).subtract(5,'days').valueOf()
    },
    {
        id:'3',
        description: 'Dinner',
        amount:350,
        note:'',
        createdAt:moment(0).add(5,'days').valueOf()
    }
]

export default expenses