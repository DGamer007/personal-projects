import { expect } from '@jest/globals'
import expensesReducer from '../../src/reducers/expenses'
import expenses from '../fixtures/expenses'

test('Should set Default State', () => {
    const state = expensesReducer(undefined, { type: '@@INIT' })

    expect(state).toEqual([])
})

test('Should Remove Expense by id', () => {
    const action = {
        type: 'REMOVE_EXPENSE',
        id: expenses[2].id
    }

    const state = expensesReducer(expenses, action)
    expect(state).toEqual([expenses[0], expenses[1]])
})

test('Should not Remove Expense if Expense not found by id', () => {
    const action = {
        type: 'REMOVE_EXPENSE',
        id: 'some!d'
    }

    const state = expensesReducer(expenses, action)
    expect(state).toEqual(expenses)
})

test('Should Add Expense', () => {
    const action = {
        type: 'ADD_EXPENSE',
        expense: {
            id: '123456-98123d',
            description: 'Evening Coffee',
            amount: 200,
            note: '',
            createdAt: 20000
        }
    }

    const state = expensesReducer(expenses, action)

    expect(state).toEqual([...expenses, action.expense])
})

test('Should Edit Expense by id', () => {
    const action = {
        type: 'EDIT_EXPENSE',
        id: expenses[0].id,
        updates: {
            amount: 300
        }
    }

    const state = expensesReducer(expenses, action)
    expect(state[0].amount).toBe(action.updates.amount)
})

test('Should not Edit Expense if Expense not found by id', () => {
    const action = {
        type: 'EDIT_EXPENSE',
        id: 'some!d',
        updates: {
            description: 'New Description'
        }
    }

    const state = expensesReducer(expenses, action)
    expect(state).toEqual(expenses)
})

test('Should Set Expenses', () => {
    const action = {
        type: 'SET_EXPENSES',
        expenses: [expenses[1]]
    }

    const state = expensesReducer(expenses, action)
    expect(state).toEqual([expenses[1]])
})