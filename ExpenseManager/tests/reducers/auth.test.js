import { expect } from "@jest/globals"
import authReducer from "../../src/reducers/auth"

test('Should set Uid for Login', () => {
    const action = {
        type: 'LOGIN',
        uid: '123abc'
    }

    const state = authReducer({}, action)
    expect(state.uid).toBe(action.uid)
})

test('Should clear Uid Logout', () => {
    const action = {
        type: 'LOGOUT'
    }

    const state = authReducer({ uid: '123abc' }, action)
    expect(state).toEqual({})
})