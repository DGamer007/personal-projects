import moment from 'moment'
import filtersReducer from '../../src/reducers/filters'

test('Should setup default filter values',()=>{
    const state = filtersReducer(undefined,{type:'@@INIT'})
    expect(state).toEqual({
        text:'',
        sortBy:'',
        startDate:moment().startOf('month'),
        endDate:moment().endOf('month')
    })
})

test('Should set sortBy to Amount',()=>{
    const action = {
        type:'SET_SORT_BY',
        sortBy:'amount'        
    }

    const state = filtersReducer(undefined,action)
    expect(state.sortBy).toBe('amount')
})

test('Should set sortBy to Date',()=>{
    const action = {
        type:'SET_SORT_BY',
        sortBy:'date'
    }

    const state = filtersReducer(undefined,action)
    expect(state.sortBy).toBe('date')
})

test('Should set Text filter',()=>{
    const action = {
        type:'SET_TEXT_FILTER',
        text:'someText'
    }

    const state = filtersReducer(undefined,action)

    expect(state.text).toBe('someText')
})

test('Should set StartDate filter',()=>{
    const startDate = moment()
    const action = {
        type:'SET_START_DATE',
        startDate
    }

    const state = filtersReducer(undefined,action)
    expect(state.startDate).toBe(startDate)
})

test('Should set EndDate filter',()=>{
    const endDate = moment()
    const action = {
        type:'SET_END_DATE',
        endDate
    }

    const state = filtersReducer(undefined,action)
    expect(state.endDate).toBe(endDate)
})