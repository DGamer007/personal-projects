import moment from "moment"
import { setEndDate, setStartDate, setTextFilter, sortByAmount, sortByDate } from "../../src/actions/filters"

test('Should return Text Filter Action object',()=>{
    const action = setTextFilter('someText')

    expect(action).toEqual({
        type:'SET_TEXT_FILTER',
        text: 'someText'
    })
})

test('Should return Text Filter Action object (without passingn Arguement',()=>{
    const action = setTextFilter()

    expect(action).toEqual({
        type:'SET_TEXT_FILTER',
        text: ''
    })
})

test('Should return Sort by Date Action object',()=>{
    const action = sortByDate()

    expect(action).toEqual({
        type:'SET_SORT_BY',
        sortBy:'date'
    })
})

test('Should return Sort by Amount Action object',()=>{
    const action = sortByAmount()

    expect(action).toEqual({
        type:'SET_SORT_BY',
        sortBy:'amount'
    })
})

test('Should return Set Start Date Action object',()=>{
    const action = setStartDate(moment(0))

    expect(action).toEqual({
        type:'SET_START_DATE',
        startDate:moment(0)
    })
})

test('Should return Set End Date Action object',()=>{
    const action = setEndDate(moment(0))

    expect(action).toEqual({
        type:'SET_END_DATE',
        endDate:moment(0)
    })
})