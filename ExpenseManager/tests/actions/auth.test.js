import { login, logout, startLogin, startLogout } from "../../src/actions/auth"

test('Should return Login Action Object', () => {
    const uid = '123abc'
    const action = login(uid)

    expect(action).toEqual({
        type: 'LOGIN',
        uid
    })
})

test('Should return Logout Action Object', () => {
    const action = logout()

    expect(action).toEqual({
        type: 'LOGOUT'
    })
})