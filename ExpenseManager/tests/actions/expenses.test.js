import { expect } from '@jest/globals'
import { startAddExpense, startSetExpenses, setExpenses, addExpense, removeExpense, editExpense, startRemoveExpense, startEditExpense } from '../../src/actions/expenses'
import expenses from '../fixtures/expenses'
import configureMockStore from 'redux-mock-store'
import thunk from 'redux-thunk'
import { database } from '../../firebase/firebase'

const createMockStore = configureMockStore([thunk])
const uid = 'TestUserID'
const defaultAuthState = { auth: { uid } }

beforeEach((done) => {
    const expensesData = {}
    expenses.forEach(({ id, description, amount, createdAt, note }) => {
        expensesData[id] = {
            description,
            amount,
            createdAt,
            note
        }
    })
    database.ref(`users/${uid}/expenses`).set(expensesData).then(() => done())
})

test('Should return Remove Expense Action object', () => {
    const action = removeExpense('12345abcde')
    expect(action).toEqual({
        type: 'REMOVE_EXPENSE',
        id: '12345abcde'
    })
})

test('Should Remove Expense from Database', (done) => {
    const store = createMockStore(defaultAuthState)
    const id = expenses[1].id

    store.dispatch(startRemoveExpense(id)).then(() => {
        const actions = store.getActions()

        expect(actions[0]).toEqual({
            type: 'REMOVE_EXPENSE',
            id
        })

        return database
            .ref(`users/${uid}/expenses/${id}`)
            .once('value')
    }).then((snapshot) => {
        expect(snapshot.val()).toBeFalsy()
        done()
    })

})

test('Should return Edit Expense Action object', () => {
    const action = editExpense('12345abcde', { amount: 200 })

    expect(action).toEqual({
        type: 'EDIT_EXPENSE',
        id: '12345abcde',
        updates: {
            amount: 200
        }
    })
})

test('Should Update Expense to Database', (done) => {
    const store = createMockStore(defaultAuthState)
    const id = expenses[1].id
    const updates = {
        amount: 100
    }

    store.dispatch(startEditExpense(id, updates)).then(() => {
        const actions = store.getActions()

        expect(actions[0]).toEqual({
            type: 'EDIT_EXPENSE',
            id,
            updates
        })

        return database
            .ref(`users/${uid}/expenses/${id}`)
            .once('value')
    }).then((snapshot) => {
        expect(snapshot.val().amount).toBe(updates.amount)
        done()
    })
})

test('Should return Add Expense Action object', () => {
    const action = addExpense(expenses[0])
    expect(action).toEqual({
        type: 'ADD_EXPENSE',
        expense: expenses[0]
    })
})

test('Should Add Expense to Database', (done) => {
    const store = createMockStore(defaultAuthState)

    const expenseData = {
        description: 'Lunch',
        amount: 500,
        createdAt: 10238203,
        note: 'At Taj'
    }

    store.dispatch(startAddExpense(expenseData)).then(() => {
        const actions = store.getActions()
        expect(actions[0]).toEqual({
            type: 'ADD_EXPENSE',
            expense: {
                id: expect.any(String),
                ...expenseData
            }
        })

        return database
            .ref(`users/${uid}/expenses/${actions[0].expense.id}`)
            .once('value')
    }).then((snapshot) => {
        expect(snapshot.val()).toEqual(expenseData)
        done()
    })
})

test('Should Add Expense to Database with default values', (done) => {
    const store = createMockStore(defaultAuthState)

    const expenseData = {
        description: '',
        amount: 0,
        createdAt: 0,
        note: ''
    }

    store.dispatch(startAddExpense()).then(() => {
        const actions = store.getActions()
        expect(actions[0]).toEqual({
            type: 'ADD_EXPENSE',
            expense: {
                id: expect.any(String),
                ...expenseData
            }
        })

        return database
            .ref(`users/${uid}/expenses/${actions[0].expense.id}`)
            .once('value')
    }).then((snapshot) => {
        expect(snapshot.val()).toEqual(expenseData)
        done()
    })
})

test('Should return SetExpenses Action Object', () => {
    const action = setExpenses(expenses)
    expect(action).toEqual({
        type: 'SET_EXPENSES',
        expenses
    })
})

test('Should Fetch Expenses from Database', (done) => {
    const store = createMockStore(defaultAuthState)
    store.dispatch(startSetExpenses()).then(() => {
        const actions = store.getActions()
        expect(actions[0]).toEqual({
            type: 'SET_EXPENSES',
            expenses
        })
        done()
    })
})