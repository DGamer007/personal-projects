# Expense Manager

A pure ReactJS Web Application,which will keep track of user's expenses.

## Overview

This Web Application uses **React** Framework to display things and **Redux** to keep track of data in React Components. Also, It has **Google Authentication System** included in it so User can directly login using his/her Google Account. All the User's data will be stored at **Firebase** Database and it has also been setup with Rules so Unauthorized accesser wont be able to access that data.
This Project also contains **Jest** Framework to test all of the components along with their actions and all the test cases are provided in Project itself.
And That's not it, This ReactJS Project is not using **create-react-app** functionality, so it also includes **Webpack** and **Babel** Configuration files.

#### Contributors:

> [Dhruv Prajapati](https://github.com/DGamer007)
