import uuid from 'uuid'
import { database } from '../../firebase/firebase'

// const expense = {
//     id: '',
//     description: '',
//     note: '',
//     amount: 123,
//     createdAt: 456852
// }

export const addExpense = (expense) => {
    return {
        type: 'ADD_EXPENSE',
        expense
    }
}

export const startAddExpense = (expenseData = {}) => {
    return (dispatch, getState) => {
        const uid = getState().auth.uid
        const {
            description = '',
            amount = 0,
            note = '',
            createdAt = 0
        } = expenseData

        const expense = { description, amount, note, createdAt }

        return database
            .ref(`users/${uid}/expenses`)
            .push(expense)
            .then((ref) => {
                dispatch(addExpense({
                    id: ref.key,
                    ...expense
                }))
            })
    }
}

export const removeExpense = (id) => {
    return {
        type: 'REMOVE_EXPENSE',
        id
    }
}

export const startRemoveExpense = (id) => {
    return (dispatch, getState) => {
        const uid = getState().auth.uid
        return database
            .ref(`users/${uid}/expenses/${id}`)
            .remove()
            .then(() => {
                dispatch(removeExpense(id))
            })
    }
}

export const editExpense = (id, updates) => {
    return {
        type: 'EDIT_EXPENSE',
        id,
        updates
    }
}

export const startEditExpense = (id, updates) => {
    return (dispatch, getState) => {
        const uid = getState().auth.uid
        return database
            .ref(`users/${uid}/expenses/${id}`)
            .update(updates)
            .then(() => {
                dispatch(editExpense(id, updates))
            })
    }
}

export const setExpenses = (expenses) => {
    return {
        type: 'SET_EXPENSES',
        expenses
    }
};

export const startSetExpenses = () => {
    return (dispatch, getState) => {
        const uid = getState().auth.uid
        return database
            .ref(`users/${uid}/expenses`)
            .once('value')
            .then((snapshot) => {
                const expenses = []

                snapshot.forEach((child) => {
                    expenses.push({
                        id: child.key,
                        ...child.val()
                    })
                })
                dispatch(setExpenses(expenses))
            })
    }
};