// const filter = {
//     text: '',
//     sortBy: '', // date or amount
//     startDate: 123,
//     endDate: 123
// }

export const setTextFilter = (text = '') => {
    return {
        type: 'SET_TEXT_FILTER',
        text
    }
}

export const sortByDate = () => {
    return {
        type: 'SET_SORT_BY',
        sortBy: 'date'
    }
}

export const sortByAmount = () => {
    return {
        type: 'SET_SORT_BY',
        sortBy: 'amount'
    }
}

export const setStartDate = (startDate) => {
    return {
        type: 'SET_START_DATE',
        startDate
    }
}

export const setEndDate = (endDate) => {
    return {
        type: 'SET_END_DATE',
        endDate
    }
}