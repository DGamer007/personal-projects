import React from 'react'
import { Switch, Router, Route } from 'react-router-dom'
import ExpenseDashboard from '../components/ExpenseDashboard'
import AddExpense from '../components/AddExpense'
import EditExpense from '../components/EditExpense'
import Help from '../components/Help'
import PageNotFound from '../components/PageNotFound'
import LoginPage from '../components/LoginPage'
import { createBrowserHistory } from 'history'
import PrivateRoute from './PrivateRoute'
import PublicRoute from './PublicRoute'

export const history = createBrowserHistory()

const AppRouter = () => {
    return (
        <Router history={history}>
            <div>
                <Switch>
                    <PublicRoute path="/" exact={true} component={LoginPage} />
                    <PrivateRoute path="/dashboard" component={ExpenseDashboard} />
                    <PrivateRoute path="/create" component={AddExpense} />
                    <PrivateRoute path="/edit/:id" component={EditExpense} />
                    <Route component={PageNotFound} />
                </Switch>
            </div>
        </Router>
    )
}

export default AppRouter