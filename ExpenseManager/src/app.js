import React from 'react'
import ReactDOM from 'react-dom'
import 'normalize.css/normalize.css'
import { Provider } from 'react-redux'
import './styles/styles.scss'
import 'react-dates/lib/css/_datepicker.css'
import AppRouter, { history } from './routers/AppRouter'
import configureStore from './store/configureStore'
import LoadingPage from './components/LoadingPage'
import firebase from '../firebase/firebase'
import { startSetExpenses } from './actions/expenses'
import { login, logout } from './actions/auth'

const store = configureStore()

const Application = (
    <Provider store={store}>
        <AppRouter />
    </Provider>
)

let hasRendered = false

const render = () => {
    if (!hasRendered) {
        ReactDOM.render(Application, document.getElementById('app'))
        hasRendered = true
    }
}

ReactDOM.render(<LoadingPage />, document.getElementById('app'))

firebase.auth().onAuthStateChanged((user) => {
    if (user) {
        store.dispatch(login(user.uid))
        store.dispatch(startSetExpenses()).then(() => {
            render()
            if (history.location.pathname === '/') {
                history.push('/dashboard')
            }
        })
    } else {
        store.dispatch(logout())
        render()
        history.push('/')
    }
})