const { getHighScores } = require('./utils/airtable')

exports.handler = async (event, context) => {
    if (event.httpMethod !== 'GET') {
        return {
            statusCode: 405,
            body: 'Method not Allowed'
        }
    }
    try {
        const records = await getHighScores()

        return {
            statusCode: 200,
            body: JSON.stringify(records)
        }
    } catch (err) {
        console.log(err)
        return {
            statusCode: 500,
            body: JSON.stringify({ Error: 'Error Occured' })
        }
    }
}