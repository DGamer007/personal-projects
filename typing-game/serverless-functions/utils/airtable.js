require('dotenv').config()
const Airtable = require('airtable')

Airtable.configure({
    apiKey: process.env.AIRTABLE_API_KEY
})

const base = Airtable.base(process.env.AIRTABLE_BASE)

const table = base.table(process.env.AIRTABLE_TABLE)

const getHighScores = async () => {
    const records = await table.select({
        sort: [{ field: 'Score', direction: 'desc' }],
        filterByFormula: 'AND(Name != "" , Score >= 0)'
    }).firstPage()

    const formattedRecords = records.map(record => ({
        id: record.id,
        fields: record.fields
    }))

    return formattedRecords
}

const createRecord = async (fields) => {
    return table.create([{ fields }])
}

const updateRecord = async (id, fields) => {
    return table.update([{ id, fields }])
}

module.exports = { table, getHighScores, createRecord, updateRecord }