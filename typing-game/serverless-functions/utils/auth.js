const jwt = require('jsonwebtoken')
const jwks = require('jwks-rsa')

const jwksClient = jwks({
    jwksUri: `https://${process.env.REACT_APP_AUTH0_DOMAIN}/.well-known/jwks.json`
})

let signingKey;

const validateAccessToken = async (token) => {
    if (!signingKey) {
        const key = await jwksClient.getSigningKey(process.env.AUTH0_KEY_ID)
        signingKey = key.getPublicKey()
    }

    const decoded = jwt.verify(token, signingKey)
    return decoded
}

const getAccessTokenFromHeaders = (headers) => {
    const rawAuthorization = headers.authorization;

    if (!rawAuthorization) { return null }

    const accessToken = rawAuthorization.replace('Bearer ', '')

    return accessToken
}

module.exports = { getAccessTokenFromHeaders, validateAccessToken }