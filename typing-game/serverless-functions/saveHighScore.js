const { getHighScores, createRecord, updateRecord } = require('./utils/airtable')
const { getAccessTokenFromHeaders, validateAccessToken } = require('./utils/auth')

exports.handler = async (event, context) => {
    let user
    try {
        const token = getAccessTokenFromHeaders(event.headers)
        if (token) {
            user = await validateAccessToken(token)
            if (!user) throw 401
        } else throw 401
    } catch (err) {
        return {
            statusCode: 401,
            body: 'Unauthorized'
        }
    }

    if (event.httpMethod !== 'POST') {
        return {
            statusCode: 405,
            body: 'Method not Allowed'
        }
    }

    const Name = user['https://typing-game/username']

    try {
        const records = await getHighScores()
        const { Score } = JSON.parse(event.body)

        if (records.length < 10) {
            await createRecord({ Score, Name })
            return { statusCode: 202 }
        } else {
            if (Score > records[9].fields.Score) {
                await updateRecord(records[9].id, { Score, Name })
                return { statusCode: 202 }
            }

            return { statusCode: 200 }
        }

    } catch (err) {
        return {
            statusCode: 500,
            body: JSON.stringify({ Error: 'Error Occured' })
        }
    }
}