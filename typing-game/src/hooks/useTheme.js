import { useEffect, useState } from 'react'

export default () => {
    const [theme, setTheme] = useState('Light')

    useEffect(() => {
        const localStorageTheme = localStorage.getItem('theme')
        setTheme(localStorageTheme || 'Light')
    }, [])

    const toggleTheme = () => {
        if (theme === 'Light') {
            setTheme('Dark')
            localStorage.setItem('theme', 'Dark')
        } else {
            setTheme('Light')
            localStorage.setItem('theme', 'Light')
        }
    }

    return [theme, toggleTheme]
}