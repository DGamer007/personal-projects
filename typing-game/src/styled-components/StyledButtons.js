import styled from 'styled-components'

export const AuthButton = styled.button`
    font-size:1.2rem;
    transition: color 200ms;
    background: none;
    border: none;
    padding: 0;
    cursor: pointer;

    &:hover{        
        color: var(--accent-color);
    }
`

export const ThemeButton = styled.button`
    font-size: 1rem;
    border: none;
    background-color: var(--main-text-color);
    color: var(--main-bg-color);
    padding: 0.2rem 0.5rem;
    border-radius: 5px;
    cursor: pointer;

    &:focus{
        border-style: none;
    }

`