import styled from 'styled-components'

export const StyledScoreMessage = styled.p`
    text-align:center;
    background-color: var(--accent-color);
    width: 50%;
    padding: 10px 0;
    margin:0 auto;
    border-radius:20px;
    color: white;
    margin-bottom: 3rem;
`

export const LinkContainer = styled.div`
    display:flex;
    flex-direction: row;
    justify-content: space-between;
`