import styled from 'styled-components'

export const ScoresList = styled.ul`        
    list-style: none;
    /* background-color: red; */
    /* text-align: center; */
    padding-left: 0;
    width: 60%;
    margin:0 auto;
`
export const ScoresListItem = styled.li`
    font-size: 1.6rem;
    margin-bottom: 0.5rem;
    display: flex;
    flex-direction: row;
    justify-content: space-between;
`