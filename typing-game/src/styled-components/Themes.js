const sharedStyled = {
    accent: '#e16365'
}

export const DarkTheme = {
    mainBgColor: '#333',
    mainTextColor: '#f9f9f9',
    ...sharedStyled
}

export const LightTheme = {
    mainBgColor: '#f9f9f9',
    mainTextColor: '#333',
    ...sharedStyled
}