import { Link } from 'react-router-dom'
import { StyledNavBar, StyledNavBrand, StyledNavItems, StyledLink } from '../styled-components/NavBar'
import { Accent } from '../styled-components/Random'
import { useAuth0 } from '@auth0/auth0-react'
import { AuthButton, ThemeButton } from '../styled-components/StyledButtons'

const NavBar = ({ toggleTheme }) => {

    const { isAuthenticated, loginWithRedirect, logout } = useAuth0()

    return (
        <StyledNavBar>
            <StyledNavBrand>
                <Link to="/">
                    Learn.Build.<Accent>Type.</Accent>
                </Link>
            </StyledNavBrand>
            <StyledNavItems>
                <li>
                    <StyledLink to="/">Home</StyledLink>
                </li>
                <li>
                    <StyledLink to="/highscore">HighScores</StyledLink>
                </li>
                <li>
                    {
                        isAuthenticated ? <AuthButton onClick={logout}>Logout</AuthButton> : <AuthButton onClick={loginWithRedirect}>Login</AuthButton>
                    }
                </li>
                <li>
                    <ThemeButton onClick={toggleTheme}>Toggle Theme</ThemeButton>
                </li>
            </StyledNavItems>
        </StyledNavBar>
    )
}

export default NavBar