import LoaderImage from '../styled-components/Loader'

const Loader = () => {
    return (
        <LoaderImage>
            <img src="/assets/Loader.gif" />
        </LoaderImage>
    )
}

export default Loader