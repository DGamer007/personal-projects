import { BrowserRouter, Switch, Route } from 'react-router-dom'
import Game from './pages/Game'
import GameOver from './pages/GameOver'
import HighScore from './pages/HighScore'
import Home from './pages/Home'
import NavBar from './components/NavBar'
import Container from './styled-components/Container'
import Main from './styled-components/Main'
import Global from './styled-components/Global'
import { useAuth0 } from '@auth0/auth0-react'
import { ThemeProvider } from 'styled-components'
import { LightTheme, DarkTheme } from './styled-components/Themes'
import useTheme from './hooks/useTheme'
import Loader from './components/Loader'

function App() {

  const { isLoading } = useAuth0()
  const [theme, toggleTheme] = useTheme()

  return (
    <BrowserRouter>
      <ThemeProvider theme={theme === 'Light' ? LightTheme : DarkTheme}>
        <Global />
        <Main>
          {
            isLoading ? <Loader /> :
              (
                <Container>
                  <NavBar toggleTheme={toggleTheme} />
                  <Switch>
                    <Route path='/' exact component={Home} />
                    <Route path='/game' component={Game} />
                    <Route path='/gameover' component={GameOver} />
                    <Route path='/highscore' component={HighScore} />
                  </Switch>
                </Container>
              )
          }
        </Main>
      </ThemeProvider>
    </BrowserRouter>
  );
}

export default App;
