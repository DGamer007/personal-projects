import { useEffect, useState } from "react"
import { useScore } from "../contexts/ScoreContext"
import { StyledLink } from "../styled-components/NavBar"
import { StyledCharacter } from "../styled-components/Game"
import { StyledScoreMessage, LinkContainer } from "../styled-components/GameOver"
import { StyledSubTitle } from "../styled-components/Random"
import { useAuth0 } from "@auth0/auth0-react"


const GameOver = ({ history }) => {

    const [score, setScore] = useScore()
    const [scoreMessage, setScoreMessage] = useState(null)

    const { getAccessTokenSilently, isAuthenticated } = useAuth0()

    if (score === -1) {
        history.push('/')
    }

    const saveHighScore = async () => {
        try {
            const token = await getAccessTokenSilently()
            const options = {
                method: 'POST',
                body: JSON.stringify({ Score: score }),
                headers: {
                    "Authorization": `Bearer ${token}`
                }
            }

            const res = await fetch('/.netlify/functions/saveHighScore', options)

            if (res.status === 202) {
                setScoreMessage('You got the HighScore.')
            } else {
                setScoreMessage('Sorry, Not the HighScore.')
            }

        } catch (error) {
            console.error(error)
        }
    }

    useEffect(() => {
        document.title = 'Game Over | Typing Page'
        if (isAuthenticated && score >= 0) {
            saveHighScore()
        }
    }, [])

    return (
        <>
            <StyledSubTitle>Game Over</StyledSubTitle>
            <StyledCharacter>{score}</StyledCharacter>
            {!isAuthenticated ? <StyledScoreMessage>LogIn or SignUp to compete for High Scores.</StyledScoreMessage> : null}
            {scoreMessage ? <StyledScoreMessage>{scoreMessage}</StyledScoreMessage> : null}
            <LinkContainer>
                <StyledLink to="/">Go Home</StyledLink>
                <StyledLink to="/game">Play Again?</StyledLink>
            </LinkContainer>
        </>
    )
}

export default GameOver