import { useEffect, useState } from 'react'
import { StyledSubTitle } from '../styled-components/Random'
import { ScoresList, ScoresListItem } from '../styled-components/HighScore'

const HighScore = () => {

    const [highScores, setHighScores] = useState([])

    useEffect(() => {
        document.title = 'HighScores | Typing Game'
        const loadHighScores = async () => {
            try {
                const res = await fetch('/.netlify/functions/getHighScores')
                const data = await res.json()

                setHighScores(data)
            } catch (error) {
                console.log(error)
            }
        }

        loadHighScores()
    }, [])

    return (
        <div>
            <StyledSubTitle>HighScores</StyledSubTitle>
            <ScoresList>
                {
                    highScores.map(score => (
                        <ScoresListItem>
                            <p>{score.fields.Name}</p>
                            <p>{score.fields.Score}</p>
                        </ScoresListItem>
                    ))
                }
            </ScoresList>
        </div>
    )
}

export default HighScore