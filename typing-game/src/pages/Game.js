import { useCallback, useEffect, useState } from 'react'
import { StyledGame, StyledCharacter, StyledScore, StyledTimer } from '../styled-components/Game'
import { Strong } from '../styled-components/Random'
import { useScore } from '../contexts/ScoreContext'

const Game = ({ history }) => {
    const MAX_SECONDS = 5
    const CHARS = 'abcdefghijklmnopqrstuvwxyz0123456789'

    const [currentChar, setCurrentChar] = useState('')
    const [score, setScore] = useScore()
    const [ms, setMs] = useState(0)
    const [seconds, setSeconds] = useState(MAX_SECONDS)

    useEffect(() => {
        document.title = 'Play | Typing Game'
        setRandomCharacter()
        setScore(0)
        const currentTime = new Date()
        const interval = setInterval(() => updateTime(currentTime), 1)

        return () => clearInterval(interval)
    }, [])

    useEffect(() => {
        if (seconds <= -1) {
            history.push('/gameover')
        }
    }, [seconds, ms])

    const setRandomCharacter = () => {
        const randomIndex = Math.floor(Math.random() * 36)
        setCurrentChar(CHARS[randomIndex])
    }

    const keyUpHandler = useCallback((e) => {
        if (e.key === currentChar) {
            setScore(prevScore => prevScore + 1)
        } else {
            if (score > 0) {
                setScore(prevScore => prevScore - 1)
            }
        }
        setRandomCharacter()
    }, [currentChar])

    useEffect(() => {
        document.addEventListener('keyup', keyUpHandler)

        return () => {
            document.removeEventListener('keyup', keyUpHandler)
        }
    }, [keyUpHandler])

    const updateTime = (startDate) => {
        const endDate = new Date()
        const elapsedTimeString = (endDate.getTime() - startDate.getTime()).toString()
        const formattedElapsedTimeString = ('0000' + elapsedTimeString).slice(-5)

        const remainingTimeSeconds = MAX_SECONDS - parseInt(formattedElapsedTimeString.substring(0, 2)) - 1
        const remainingTimeMiliSeconds = 1000 - parseInt(formattedElapsedTimeString.substring(formattedElapsedTimeString.length - 3))

        setSeconds(addLeadingZeros(remainingTimeSeconds, 2))
        setMs(addLeadingZeros(remainingTimeMiliSeconds, 3))
    }

    const addLeadingZeros = (val, length) => {
        let zeros = ''
        for (let i = 0; i < length; i++) {
            zeros += '0'
        }

        return (zeros + val).slice(-length)
    }

    return (
        <StyledGame>
            <StyledScore>Score : <Strong>{score}</Strong></StyledScore>
            <StyledCharacter>{currentChar}</StyledCharacter>
            <StyledTimer>Time : <Strong>{seconds}:{ms}</Strong></StyledTimer>
        </StyledGame>
    )
}

export default Game