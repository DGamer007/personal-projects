import { useEffect } from 'react'
import CTA from '../styled-components/CTA'
import { Accent, StyledTitle } from '../styled-components/Random'

const Home = () => {

    useEffect(() => {
        document.title = 'Home | Typing Game'
    }, [])

    return (
        <div>
            <StyledTitle>Ready to type?</StyledTitle>
            <CTA to="/game">Click or type <Accent>'s'</Accent> to start playing!</CTA>
        </div>
    )
}

export default Home