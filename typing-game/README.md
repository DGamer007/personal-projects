# Typing Game

A React Web Application which will allow users to practice their typing skills and improve it by some point.

## Overview

This is a Single Page React Web Application. This Project consists of many interesting things such as It is using **Serverless Functions** (On Netlify), It has **Styled Components** for reusability and easy styling of Components, It is using **Auth0** Service for User Authentication and Serverless Function APIs' Authorization, It is utilizing the **Airtable** Service for Storing and Retrieving Users' Score Data, It has **Theme** functionality implimented in it too. While working on the Project I used **Continuous Integration** with Github Repository.

#### Contributors:

> [Dhruv Prajapati](https://github.com/DGamer007)
