# Weather-App

A complete Node.js Web Application, which will display Weather Information for location that has been provided by user.

# Weather-App-API

This application also provides API to get the weather information for any location possible.

## Used APIs:

1. [Weather Stack API](https://weatherstack.com/documentation)

2. [Mapbox Geocoding API](https://docs.mapbox.com/api/search/geocoding/)

#### Contributors:

> [Dhruv Prajapati](https://github.com/DGamer007)
