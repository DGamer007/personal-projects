enum ReportType {
    INCOME = 'income',
    EXPENSE = 'expense'
}

type Report = {
    id: string;
    source: string;
    amount: number;
    created_at: Date;
    updated_at: Date;
    type: ReportType;
};

interface DataBase {
    reports: Report[];
}

export { ReportType, Report, DataBase };