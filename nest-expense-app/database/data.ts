import { DataBase, ReportType } from './data.types';

const data: DataBase = {
    reports: [
        {
            id: 'uuid1',
            source: 'Salary',
            amount: 1000,
            created_at: new Date(),
            updated_at: new Date(),
            type: ReportType.INCOME
        },
        {
            id: 'uuid2',
            source: 'Youtube',
            amount: 2000,
            created_at: new Date(),
            updated_at: new Date(),
            type: ReportType.INCOME
        },
        {
            id: 'uuid3',
            source: 'Food',
            amount: 200,
            created_at: new Date(),
            updated_at: new Date(),
            type: ReportType.EXPENSE
        }
    ]
};

export default data;