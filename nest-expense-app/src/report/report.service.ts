import { Injectable } from '@nestjs/common';
import { v4 as uuid } from 'uuid';
import { CreateReport, ReportResponseDto, UpdateReport } from 'src/dtos/report.controller.dto';
import data from 'database/data';
import { ReportType } from 'database/data.types';

@Injectable()
export class ReportService {
    getAllReports(type: string): ReportResponseDto[] {
        return data
            .reports
            .filter(report => report.type === type)
            .map(report => new ReportResponseDto(report));
    }

    getReportById(type: string, id: string): ReportResponseDto {
        const report = data
            .reports
            .find(report => report.type === type && report.id === id);

        if (!report) return null;

        return new ReportResponseDto(report);
    }

    createReport(type: string, body: CreateReport): ReportResponseDto {
        const report = {
            id: uuid(),
            ...body,
            type: type === 'income' ? ReportType.INCOME : ReportType.EXPENSE,
            created_at: new Date(),
            updated_at: new Date(),
        };

        data.reports.push(report);
        return new ReportResponseDto(report);
    };

    updateReport(type: string, id: string, body: UpdateReport): ReportResponseDto {
        const reportIndex = data.reports.findIndex(report => report.type === type && report.id === id);
        if (reportIndex === -1) return null;

        const updatedReport = {
            ...data.reports[reportIndex],
            ...body,
            updated_at: new Date()
        };

        data.reports[reportIndex] = updatedReport;
        return new ReportResponseDto(updatedReport);
    }

    deleteReport(type: string, id: string) {
        const reportIndex = data
            .reports
            .findIndex(report => report.type === type && report.id === id);

        if (reportIndex === -1) return null;

        data.reports.splice(reportIndex, 1);
        return;
    }
}
