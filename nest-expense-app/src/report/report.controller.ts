import {
    Controller,
    Get,
    ParseEnumPipe,
    Param,
    ParseUUIDPipe,
    Post,
    Body,
    Delete,
    Put
} from '@nestjs/common';
import { ReportType } from 'database/data.types';
import { ReportResponseDto, CreateReportDto, UpdateReportDto } from 'src/dtos/report.controller.dto';
import { ReportService } from './report.service';

@Controller('report/:type')
export class ReportController {

    constructor(private readonly reportService: ReportService) { }

    @Get()
    getAllReports(
        @Param('type', new ParseEnumPipe(ReportType)) type: string,
    ): ReportResponseDto[] {
        return this.reportService.getAllReports(type);
    }

    @Get(':id')
    getReportById(
        @Param('type', new ParseEnumPipe(ReportType)) type: string,
        @Param('id', ParseUUIDPipe) id: string
    ): ReportResponseDto {
        return this.reportService.getReportById(type, id);
    }

    @Post()
    createReport(
        @Param('type', new ParseEnumPipe(ReportType)) type: string,
        @Body() body: CreateReportDto,
    ): ReportResponseDto {
        return this.reportService.createReport(type, body);
    }

    @Put(':id')
    updateReport(
        @Param('type', new ParseEnumPipe(ReportType)) type: string,
        @Param('id', ParseUUIDPipe) id: string,
        @Body() body: UpdateReportDto

    ): ReportResponseDto {
        return this.reportService.updateReport(type, id, body);
    }

    @Delete(':id')
    deleteReport(
        @Param('type', new ParseEnumPipe(ReportType)) type: string,
        @Param('id', ParseUUIDPipe) id: string
    ): ReportResponseDto {
        return this.reportService.deleteReport(type, id);
    }
}
