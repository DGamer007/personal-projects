import { Exclude, Expose } from 'class-transformer';
import { IsNumber, IsNotEmpty, IsOptional, IsPositive, IsString } from 'class-validator';
import { ReportType } from 'database/data.types';

export class ReportResponseDto {
    id: string;
    amount: number;
    source: string;
    type: ReportType;

    @Expose({ name: "createdAt" })
    getCreatedAt() {
        return this.created_at;
    }

    @Exclude()
    created_at: Date;

    @Exclude()
    updated_at: Date;

    constructor(partial: Partial<ReportResponseDto>) {
        Object.assign(this, partial);
    }
}

export class CreateReportDto {

    @IsNumber()
    @IsPositive()
    amount: number;

    @IsString()
    @IsNotEmpty()
    source: string;
}

export class UpdateReportDto {

    @IsNumber()
    @IsPositive()
    @IsOptional()
    amount: number;

    @IsString()
    @IsNotEmpty()
    @IsOptional()
    source: string;
}

export type CreateReport = {
    amount: number;
    source: string;
};

export type UpdateReport = {
    amount?: number;
    source?: string;
};