import { CallHandler, ExecutionContext, NestInterceptor } from '@nestjs/common';
import { map } from 'rxjs';

// Custom Interceptor to Transform the Response
export class CustomInterceptor implements NestInterceptor {
    intercept(context: ExecutionContext, next: CallHandler) {
        return next.handle().pipe(
            map(data => {
                if (Array.isArray(data)) {
                    return data.map(report => this.transformData(report));
                }
                return this.transformData(data);
            })
        );
    }

    private transformData(data) {
        const response = {
            ...data,
            createdAt: data.created_at
        };

        delete response.created_at;
        delete response.updated_at;

        return response;
    }
}