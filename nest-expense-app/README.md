# Nest-Expense-App

A Simple Nest.js Expense Manager Application for users to keep of their expenses.

## Overview

This is a Basic Nest.js Application Which provides Rest APIs to keep track of Users' expenses by storing that data in Non-Persistant way.

#### Contributors:

> [Dhruv Prajapati](https://github.com/DGamer007)